# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "3.3.0"
  constraints = "~> 3.0"
  hashes = [
    "h1:g1YjGWuiCcBwv5YcIWFcjL+KU5ZHszUINozNW0L1HLg=",
    "zh:1e41b566dd2a1dd8cdb4ee35e4821c453f079499738ce3229e8f1ca2ad312eb1",
    "zh:3aecc2c975f84452c8eb46eaff14fe7cd95b6f31aaee739f61ebfb02fef265ff",
    "zh:3c1c57526a1867893587d8bdb8c643ce57c27f3490100c93e63ee66398eec475",
    "zh:419d4172b0eef4cc7a27f7c15e790442f595aaa87ef54362cd9578cae7b9b55d",
    "zh:5de73c23e8a1273fb6c62f1c6683e7a2983b00a5a295b3f793115b7cd198a189",
    "zh:6e8c4772dfc7c4c7a455abafcb27eae24b7fe315259d94b3a8f7fb648b12eefa",
    "zh:b6e999f00d4e53a143ca339af165ec68833c63623b05f6840a4384ef2bcc70dc",
    "zh:bab4ee5cf949db4c76192a878aeb04fc9a061c7c783595ff7aff2a6ef35ddce7",
    "zh:c9d65cea47686ff37efa03be8e88132f86a5516a9154fa1c99160bed0c582a9d",
    "zh:fb6b9250a2f9ebaf21b6b1a15032b79a3c0fa64897e4498ea0db589554ee61e5",
    "zh:fcbea1ec35308706e5795f2360c82dfe51c556677d8ad2249ed6cbb44e486cba",
  ]
}

provider "registry.terraform.io/hashicorp/azurerm" {
  version = "2.64.0"
  hashes = [
    "h1:gW4rnRwjikLc+28Iw1R2iFFLh0q4PDA8o063u8XsrK4=",
    "zh:048da64c3d173f3467e908ca8b28962bbf6e3e06597614474b46d4c09ec8ca6c",
    "zh:08d5baa31e498b2e7761d88a8ff5875066566f9b3644a5c12c8ea305e1a7c85d",
    "zh:0a452f95795f56c16f5b0febe05539f44638895f387973f594ac3de179f22150",
    "zh:1b0dd54a023ef22c9fadea2cb6e8e66e2c9a29d23921706e10b35b1bd2a47ed3",
    "zh:3260fdc14d2a33c0cc0e7b230687d303e12852c54aea1120918e7b77f954ed1b",
    "zh:b36dd823f543fb45b31a623ff68be5fd49fddcb50c2e032dd44828a26f9d41b9",
    "zh:ba6514590b1be102438cc3632795965f3e271044635bf05ec0f0e46c4795b06c",
    "zh:bf663c286ba4198111d8b9f4987a277f1a378d0ef40a824c8fc25f6e4de1866d",
    "zh:d8c122295c29c90788b4cdbcab53e1fc5c75071d6600b5389372dc8f8bda1967",
    "zh:dffbb1af6ab6d1b875cbd4fd198d3b12c28261a216c1a32f393b8c795b62bb30",
    "zh:f8dca9bf566e7869412a5c10e44f64f3a19eee7c4aa18e7ce5db9a6f145a2a4f",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version = "2.3.2"
  hashes = [
    "h1:X+wKrZNYF+qtoBNcMD3cL0+bXSWUDZ7RpHm+2vAwvhE=",
    "zh:10f71c170be13538374a4b9553fcb3d98a6036bcd1ca5901877773116c3f828e",
    "zh:11d2230e531b7480317e988207a73cb67b332f225b0892304983b19b6014ebe0",
    "zh:3317387a9a6cc27fd7536b8f3cad4b8a9285e9461f125c5a15d192cef3281856",
    "zh:458a9858362900fbe97e00432ae8a5bef212a4dacf97a57ede7534c164730da4",
    "zh:50ea297007d9fe53e5411577f87a4b13f3877ce732089b42f938430e6aadff0d",
    "zh:56705c959e4cbea3b115782d04c62c68ac75128c5c44ee7aa4043df253ffbfe3",
    "zh:7eb3722f7f036e224824470c3e0d941f1f268fcd5fa2f8203e0eee425d0e1484",
    "zh:9f408a6df4d74089e6ce18f9206b06b8107ddb57e2bc9b958a6b7dc352c62980",
    "zh:aadd25ccc3021040808feb2645779962f638766eb583f586806e59f24dde81bb",
    "zh:b101c3456e4309b09aab129b0118561178c92cb4be5d96dec553189c3084dca1",
    "zh:ec08478573b4953764099fbfd670fae81dc24b60e467fb3b023e6fab50b70a9e",
  ]
}
